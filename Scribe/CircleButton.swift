//
//  CircleButton.swift
//  Scribe
//
//  Created by Marko Bizjak on 16/11/2016.
//  Copyright © 2016 Marko Bizjak. All rights reserved.
//

import UIKit

@IBDesignable
class CircleButton: UIButton {

    @IBInspectable var cornerRadius: CGFloat = 30.0 {
        didSet{
            setupView()
        }
    }

    override func prepareForInterfaceBuilder() {
        setupView()
    }
    
    func setupView() {
        layer.cornerRadius = cornerRadius
    }
}
