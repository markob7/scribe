//
//  ViewController.swift
//  Scribe
//
//  Created by Marko Bizjak on 16/11/2016.
//  Copyright © 2016 Marko Bizjak. All rights reserved.
//

import UIKit
import Speech
import AVFoundation

class ViewController: UIViewController, AVAudioPlayerDelegate {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var transcriptionTextField: UITextView!
    
    var audioPlayer: AVAudioPlayer!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.printSomething), name:NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        

        activityIndicator.isHidden = true
    }
    
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        player.stop()
        activityIndicator.isHidden = true
        activityIndicator.stopAnimating()
    }
    
    func printSomething() {
        print("active")
    }
    
    func requestSpeechAuth() {
        SFSpeechRecognizer.requestAuthorization {authStatus in
            if authStatus == SFSpeechRecognizerAuthorizationStatus.authorized {
                if let path = Bundle.main.url(forResource: "test", withExtension: "m4a") {
                    do {
                        let sound = try AVAudioPlayer(contentsOf: path)
                        self.audioPlayer = sound
                        self.audioPlayer.delegate = self

                        sound.play()
                    } catch {
                        print(error)
                    }
                    
                    let recognizer = SFSpeechRecognizer()
                    let request = SFSpeechURLRecognitionRequest(url: path)
                    recognizer?.recognitionTask(with: request) { (result, error) in
                        
                        if error != nil {
                            print("There was an error: \(error)")
                        } else {
                            print(result?.bestTranscription.formattedString as Any)
                            self.transcriptionTextField.text = result?.bestTranscription.formattedString
                        }
                    }
                }
            }
        }
    }

    @IBAction func playButtonPressed(_ sender: Any) {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        requestSpeechAuth()
    }
    


}

